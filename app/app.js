'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngSanitize',
  'myApp.route',
  'myApp.filter',
  'myApp.home.controller',
  'myApp.blog.controller',
  'myApp.blog.factory',
  'myApp.chat.factory',
  'myApp.header',
  'myApp.footer',
  'myApp.menu',
  'myApp.banner',
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/home'});
}]);
