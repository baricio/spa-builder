<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace blog\Libraries;

/**
 * Description of imagem
 *
 * @author developer
 */

use PDO;

class BlogModel
{
    //put your code here

    public $banco = '';
    public $maxRegistros = 10;
    public $siteId = 0;
    public $paginaId = 0;
    public $debug = false;

    public $page_atual_post = 0;
    public $page_total_post = 0;
    public $registros_post = 0;

    public function __construct($params)
    {

        $this->siteId = (isset($params['siteId'])) ? $params['siteId'] : 0;
        $this->paginaId = (isset($params['paginaId'])) ? $params['paginaId'] : 0;
        $this->debug = (isset($params['debug'])) ? $params['debug'] : false;
        $this->banco = $_SERVER['DOCUMENT_ROOT'] . '/blog/site' . $this->siteId . '/pagina' . $this->paginaId . '/blog.db';

    }

    public function conect_sqlite()
    {

        $create_tables = 0;

        //verify se banco já existe
        if (!is_file($this->banco)) {
            $create_tables = 1;
        }

        //$dbhandle = sqlite_open($this->banco, 0666, $error);
        try {
            $dbhandle = new PDO('sqlite:' . $this->banco);
        } catch (Exception $e) {
            $dbhandle = null;
            $create_tables = 0;
        }

        //create table posts
        if ($create_tables) {
            $sql = "CREATE TABLE posts ";
            $sql .= "( ";
            $sql .= "id integer PRIMARY KEY,";
            $sql .= "title varchar(150),";
            $sql .= "post text,";
            $sql .= "category_id integer DEFAULT 0,";
            $sql .= "date_post date,";
            $sql .= "date_last_update timestamp,";
            $sql .= "active boolean DEFAULT 0,";
            $sql .= "key_words text,";
            $sql .= "summary text,";
            $sql .= "imagem_destaque text,";
            $sql .= "importado boolean DEFAULT 0";
            $sql .= ")";
            $dbhandle->exec($sql);

            $sql = "CREATE TABLE categorys ";
            $sql .= "( ";
            $sql .= "id integer PRIMARY KEY, ";
            $sql .= "category varchar(60)";
            $sql .= ")";
            $dbhandle->exec($sql);

            self::creteDataSqLite();
        }

        if (!$dbhandle) {
            return null;
        } else {
            return $dbhandle;
        }

    }

    public function getPostsTotal($where = '', $sql = '')
    {
        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog) {
            if (empty($sql)) {
                $sql = "SELECT * FROM POSTS";
                if (!empty($where)) {$sql .= $where;}
            }

            $result = $conn_blog->query($sql);

            if ($result) {
                return $result->rowCount();
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function deletePost($id = null)
    {
        if (!empty($id)) {
            $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

            if ($conn_blog) {
                $query = "DELETE FROM posts WHERE id=" . $id;
                $conn_blog->exec($query);

                return true;
            }
        }
        return false;
    }

    public function getPosts($paginacao = 1, $cat_id = null, $post_id = null, $title = null, $importado = null, $filtra_data = false)
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);
        $max_registros = $this->maxRegistros;
        $where = '';

        if ($conn_blog) {
            $query = "SELECT ";
            $query .= "p.id as id, p.title as title, p.post as post, p.category_id as category_id, ";
            $query .= "p.date_post as date_post, p.date_last_update as date_last_update, p.active as active, ";
            $query .= "p.key_words as key_words, p.summary as summary, c.category as category, p.imagem_destaque as imagem_destaque ";
            $query .= "FROM posts p ";
            $query .= "LEFT JOIN categorys c on p.category_id=c.id ";
            $where = "WHERE 1=1 ";

            if (is_numeric($cat_id)) {$where .= "AND p.category_id=" . $cat_id . " ";}
            if (!empty($post_id)) {$where .= "AND p.id=" . $post_id . " ";}
            if (!empty($title)) {$where .= "AND p.title like '%" . $title . "%' ";}
            if (is_numeric($importado)) {$where .= "AND p.importado=" . $importado . " ";}
            if ($filtra_data) {$where .= "AND p.date_post<=date('now')";} //pega apenas os posts da data de hoje para trás

            $query .= $where;
            $query .= "Order by date_post desc, p.id desc ";

            if (!empty($paginacao)) {
                $paginacao_ref = $paginacao - 1;
                $query .= "limit " . $max_registros . " offset " . ($max_registros * $paginacao_ref) . " ";
            }
            $result = $conn_blog->query($query);
            if ($result) {
                $dados = $result->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $dados = array();
            }
            $conn_blog = null;

            //guarda resultados de paginação
            $this->registros_post = self::getTotalPosts($where);
            $this->page_atual_post = $paginacao;
            $this->page_total_post = ceil($this->registros_post / $this->maxRegistros);

            return $dados;
        }

    }

    public function getTotalPosts($where = '')
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        $query = "SELECT ";
        $query .= "count(*) as total ";
        $query .= "FROM posts p ";
        $query .= "JOIN categorys c on p.category_id=c.id ";
        if (!empty($where)) {
            $query .= $where . " ";
        }

        $result = $conn_blog->query($query);
        if ($result) {
            $dados = $result->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($dados)) {
                return $dados[0]['total'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getLastPost()
    {
        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);
        if ($conn_blog) {
            $query = "select * from posts order by id desc limit 1";
            $result = $conn_blog->query($query);
            if ($result) {
                $dados = $result->fetch(PDO::FETCH_ASSOC);
            } else {
                $dados = array();
            }
        } else {
            $dados = array();
        }
        $conn_blog = null;
        return $dados;
    }

    public function insertPost($dados = array())
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog && !empty($dados)) {
            $sql = " INSERT INTO Posts (title, post, category_id, date_post, date_last_update, active, key_words, summary, imagem_destaque ) ";
            $sql .= " VALUES(:title, :post, :category_id, :date_post, :date_last_update, :active, :key_words, :summary, :imagem_destaque)";

            $prep = $conn_blog->prepare($sql);
            $prep->bindValue(':title', $dados['title'], PDO::PARAM_STR);
            $prep->bindValue(':post', $dados['post'], PDO::PARAM_STR);
            $prep->bindValue(':category_id', $dados['category_id'], PDO::PARAM_INT);
            $prep->bindValue(':date_post', $dados['date_post'], PDO::PARAM_STR);
            $prep->bindValue(':date_last_update', $dados['date_last_update'], PDO::PARAM_STR);
            $prep->bindValue(':active', $dados['active'], PDO::PARAM_INT);
            $prep->bindValue(':key_words', $dados['key_words'], PDO::PARAM_STR);
            $prep->bindValue(':summary', $dados['summary'], PDO::PARAM_STR);
            $prep->bindValue(':imagem_destaque', $dados['imagem_destaque'], PDO::PARAM_STR);
            $prep->execute();

            return true;
        } else {
            return false;
        }
        $conn_blog = null;

    }

    public function updatePost($id = null, $dados = array())
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog && !empty($dados) && !empty($id)) {

            $sql = " UPDATE Posts SET ";
            $sql .= " title=:title, ";
            $sql .= " post=:post, ";
            $sql .= " category_id=:category_id, ";
            $sql .= " date_post=:date_post, ";
            $sql .= " date_last_update=:date_last_update, ";
            $sql .= " active=:active, ";
            $sql .= " key_words=:key_words, ";
            $sql .= " summary=:summary, ";
            $sql .= " imagem_destaque=:imagem_destaque, ";
            $sql .= " importado=:importado ";
            $sql .= " where id=:id";

            $prep = $conn_blog->prepare($sql);
            $prep->bindValue(':title', $dados['title'], PDO::PARAM_STR);
            $prep->bindValue(':post', $dados['post'], PDO::PARAM_STR);
            $prep->bindValue(':category_id', $dados['category_id'], PDO::PARAM_INT);
            $prep->bindValue(':date_post', $dados['date_post'], PDO::PARAM_STR);
            $prep->bindValue(':date_last_update', $dados['date_last_update'], PDO::PARAM_STR);
            $prep->bindValue(':active', $dados['active'], PDO::PARAM_INT);
            $prep->bindValue(':key_words', $dados['key_words'], PDO::PARAM_STR);
            $prep->bindValue(':summary', $dados['summary'], PDO::PARAM_STR);
            $prep->bindValue(':imagem_destaque', $dados['imagem_destaque'], PDO::PARAM_STR);
            $prep->bindValue(':importado', 0, PDO::PARAM_INT);
            $prep->bindValue(':id', $id, PDO::PARAM_INT);

            return $prep->execute();

        } else {
            return false;
        }
        $conn_blog->close();

    }

    public function updatePostImportado()
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog) {
            $sql = " UPDATE Posts SET ";
            $sql .= " importado='1' ";
            $result = $conn_blog->query($sql);
            return $result;
        } else {
            return false;
        }
        $conn_blog = null;

    }

    public function insertCategory($category = null)
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog && !empty($category)) {
            $sql = "  INSERT INTO categorys (category)  values ('" . html2txt($category) . "') ";
            $result = $conn_blog->query($sql);
            return $result;
        } else {
            return false;
        }
        $conn_blog = null;

    }

    public function updateCategory($id = null, $category = array())
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog && !empty($category)) {
            $sql = " UPDATE categorys SET category='" . html2txt($category) . "' WHERE id='" . $id . "' ";
            $result = $conn_blog->query($sql);
            return $result;
        } else {
            return false;
        }
        $conn_blog = null;

    }

    public function getCategory($exibir_categorias_usadas = 1, $category_id = null, $category = null)
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        if ($conn_blog) {
            $query = "SELECT c.id as id, c.category as category, count(p.id) as total_post ";
            $query .= "FROM categorys c ";
            if ($exibir_categorias_usadas == 1) {
                $query .= "JOIN posts p on p.category_id=c.id ";
            } else {
                $query .= "LEFT JOIN posts p on p.category_id=c.id ";
            }

            $query .= "WHERE 1=1 ";

            if (is_numeric($category_id)) {$query .= "AND c.id='" . $category_id . "' ";}
            if (!empty($category)) {$query .= "AND c.category like '%" . $category . "%' ";}

            $query .= "GROUP BY c.id ";
            $query .= "Order by c.category ";

            $result = $conn_blog->prepare($query);
            if ($result) {
                $result->execute();
                $dados = $result->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $dados = array();
            }

            $conn_blog = null;
            return $dados;
        }

    }

    public function deleteCategory($id = null)
    {
        if (!empty($id)) {
            $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

            if ($conn_blog) {
                $query = "DELETE FROM categorys WHERE id=" . $id;
                $conn_blog->query($query);

                $sql = " UPDATE Posts SET ";
                $sql .= " category_id='0' ";
                $sql .= " WHERE category_id=" . $id;
                $result = $conn_blog->query($sql);

                return true;
            }
        }
    }

    public function creteDataSqLite()
    {

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);

        $post = "Este é apenas um post de exemplo, clique em gerenciar posts para altera-lo.";

        $sql = " INSERT INTO Posts (title, post, category_id, date_post, date_last_update, active, key_words, summary, imagem_destaque ) ";
        $sql .= " VALUES(:title, :post, :category_id, :date_post, :date_last_update, :active, :key_words, :summary, :imagem_destaque )";
        $prep = $conn_blog->prepare($sql);
        $prep->bindValue(':title', 'Meu primeiro post', PDO::PARAM_STR);
        $prep->bindValue(':post', $post, PDO::PARAM_STR);
        $prep->bindValue(':category_id', 1, PDO::PARAM_INT);
        $prep->bindValue(':date_post', date('Y-m-d', strtotime(' ' . (-1) . ' day')), PDO::PARAM_STR);
        $prep->bindValue(':date_last_update', date('Y-m-d', strtotime(' ' . (-1) . ' day')), PDO::PARAM_STR);
        $prep->bindValue(':active', 1, PDO::PARAM_INT);
        $prep->bindValue(':key_words', '', PDO::PARAM_STR);
        $prep->bindValue(':summary', 'Este é apenas um post de exemplo, clique em gerenciar posts para altera-lo', PDO::PARAM_STR);
        $prep->bindValue(':imagem_destaque', "http://www.fasar.edu.br/imagens/blog.jpg", PDO::PARAM_STR);

        $prep->execute();
        $conn_blog = null;

        $conn_blog = self::conect_sqlite($this->siteId, $this->paginaId);
        $sql = " INSERT INTO categorys (id,category)  values (0,'Não Definido') ";
        $conn_blog->exec($sql);
        $sql = " INSERT INTO categorys (id,category)  values (1,'Primeiro Post') ";
        $conn_blog->exec($sql);
        $conn_blog = null;

    }

}
