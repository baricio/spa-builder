<?php

namespace blog\Libraries;

require_once 'simple_html_dom.php';

class Util
{
    public static function postTranslate($posts)
    {
        foreach ($posts as $post) {
            $post['imagem_destaque'] = str_replace('[local]', SELF::base_url('/images/thumb', true), $post['imagem_destaque']);
            //$post['date_post'] = SELF::dataExtenso($post['date_post']);
            //$post['post'] = SELF::converteVideo($post['post']);
            //$post['post'] = SELF::converteImagem($post['post'], SELF::base_url('/images/', true));
        }
        return $posts;
    }

    public static function dataExtenso($time = 'now', $tipo = 1)
    {
        $hoje = strtotime($time);
        $i = getdate($hoje); // Consegue informações data/hora

        $data = $i['mday']; //Representação numérica do dia do mês (1 a 31)
        $mes = $i['mon']; // Representação numérica de um mês (1 a 12)
        $ano = $i['year']; // Ano com 4 digitos, lógico, né?
        $data = str_pad($data, 2, "0", STR_PAD_LEFT); // só para colocar um zerinho à esquerda caso seja de 1 à 9, sacou?

        if ($tipo == 2) {
            $nomemes = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
            return $data . " de " . $nomemes[($mes - 1)] . " de " . $ano;
        } else {
            $nomemes = array("JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ");
            return $data . " " . $nomemes[($mes - 1)] . " " . $ano;
        }
    }

    public static function converteVideo($html = null)
    {

        if (!empty($html)) {

            $htmlvideo = str_get_html($html);

            $div_images_video = $htmlvideo->find('[class=video_image]');

            foreach ($div_images_video as $video) {
                $video_url = (array_key_exists('video_url', $video->attr)) ? $video->attr['video_url'] : '';
                $width = (array_key_exists('width', $video->attr)) ? $video->attr['width'] . 'px' : '';
                $height = (array_key_exists('height', $video->attr)) ? $video->attr['height'] . 'px' : '';
                $style = (array_key_exists('style', $video->attr)) ? $video->attr['style'] : '';
                $video->outertext = '<iframe width="' . $width . '" height="' . $height . '" style="' . $style . '"  src="' . $video_url . '" frameborder="0" allowfullscreen></iframe>';
            }

            return $htmlvideo;

        } else {
            return '';
        }

    }

    public static function converteImagem($html = null, $pasta = null)
    {

        if (!empty($html)) {

            $htmlimagem = str_get_html($html);

            $div_images = $htmlimagem->find('[class=ref_image]');

            foreach ($div_images as $image) {
                //pre($video,1);
                $tipo = (array_key_exists('tipo', $image->attr)) ? $image->attr['tipo'] : '';
                if ($tipo == 'local') {
                    $src = (array_key_exists('src', $image->attr)) ? $image->attr['src'] : '';

                    if (!empty($pasta)) {
                        $pasta_array = explode('/', $src);
                        $src = $pasta . end($pasta_array);
                    }

                    $image->src = $src;
                }
            }

            return $htmlimagem;

        } else {
            return '';
        }

    }

    public static function base_url($url = '', $apenas_dominio = false)
    {

        $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://'; // checking if the https is enabled
        $baseUrl .= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST'); // checking adding the host name to the website address
        if (!$apenas_dominio) {
            $baseUrl .= isset($_SERVER['SCRIPT_NAME']) ? dirname($_SERVER['SCRIPT_NAME']) : dirname(getenv('SCRIPT_NAME')); // adding the directory name to the created url and then returning it.
        }
        return $baseUrl .= $url; // adding the directory name to the created url and then returning it.
    }
}
