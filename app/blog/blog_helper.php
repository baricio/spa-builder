<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/funcoes.php'; 
include_once 'bloglibrarie.php';
include_once 'simple_html_dom.php';

function getConteudoBlogResponsivo($nomePagina = null,$site_id = null,$pagina_id = null, $blog_acao = array('tipo'=>'paginacao','paginacao'=>1), $post_por_pagina = 0, $facebookActive = 0){

    $conteudo  = '<div class="container-fluid conteudo">';
    $conteudo .= '<div class="row">';
 
    //abre a coluna
    $conteudo .= '<div class="col-md-4 coluna_pequena" style="position: relative">';
    $conteudo .= '<div class="margens columns ui-sortable">';
    $conteudo .= '<div class="cx_categorias">';

    //insere os blocos de conteudo
    $conteudo .= getBlocosBlog(array('nome_pagina'=>$nomePagina,'indice'=>1,'site_id'=>$site_id,'pagina_id'=>$pagina_id),$blog_acao);
    $conteudo .= '</div></div></div>';

    //abre a coluna
    $conteudo .= '<div class="col-md-8 coluna_media" style="position: relative">';
    $conteudo .= '<div class="margens cx_posts columns ui-sortable">';
    //$conteudo .= '<div class="cx_posts">';

    //insere os blocos de conteudo
    $conteudo .= getBlocosBlog(array('nome_pagina'=>$nomePagina,'indice'=>2,'site_id'=>$site_id,'pagina_id'=>$pagina_id),$blog_acao, $post_por_pagina,$facebookActive);
    //$conteudo .= '</div>';
    
    $conteudo .= '</div>';
    $conteudo .= '</div>';
    if(isset($blog_acao["tipo"]) && $blog_acao["tipo"] == "post"){
        $conteudo .= '<a href="javascript:history.back()" class="btn_voltar_do_post">Voltar</a>';
    }
    $conteudo .= '</div>';
    $conteudo .= '</div>';

    return $conteudo;
}

function getConteudoBlog($nomePagina = null,$site_id = null,$pagina_id = null, $blog_acao = array('tipo'=>'paginacao','paginacao'=>1), $post_por_pagina = 0, $facebookActive = 0){


    $conteudo = '<div class="container_12 conteudo">';

    //abre a coluna
    $conteudo .= '<div class="grid_4 coluna_pequena" style="position: relative">';
    $conteudo .= '<div class="margens columns ui-sortable">';
    $conteudo .= '<div class="cx_categorias">';

    //insere os blocos de conteudo
    $conteudo .= getBlocosBlog(array('nome_pagina'=>$nomePagina,'indice'=>1,'site_id'=>$site_id,'pagina_id'=>$pagina_id),$blog_acao);
    $conteudo .= '</div></div></div>';

    //abre a coluna
    $conteudo .= '<div class="grid_8 coluna_media" style="position: relative">';
    $conteudo .= '<div class="margens cx_posts columns ui-sortable">';
    //$conteudo .= '<div class="cx_posts">';

    //insere os blocos de conteudo
    $conteudo .= getBlocosBlog(array('nome_pagina'=>$nomePagina,'indice'=>2,'site_id'=>$site_id,'pagina_id'=>$pagina_id),$blog_acao, $post_por_pagina,$facebookActive);
    //$conteudo .= '</div>';

    $conteudo .= '</div>';
    if(isset($blog_acao["tipo"]) && $blog_acao["tipo"] == "post"){
        $conteudo .= '<a href="javascript:history.back()" class="btn_voltar_do_post">Voltar</a>';
    }
    $conteudo .= '</div>';

    return $conteudo;
}

function getBlocosBlog($params, $blog_acao = array('tipo'=>'paginacao','paginacao'=>1), $post_por_pagina = 0,$facebookActive = 0){
   
    $nomePagina   = (array_key_exists('nome_pagina',$params))? $params['nome_pagina'] : null;
    $indice       = (array_key_exists('indice',$params))? $params['indice'] : null;
    $site_id      = (array_key_exists('site_id',$params))? $params['site_id'] : null;
    $pagina_id    = (array_key_exists('pagina_id',$params))? $params['pagina_id'] : null;
    $categoria_id = (array_key_exists('categoria_id',$params))? $params['categoria_id'] : null;
    
    $blogClass = new bloglibrarie(array('siteId'=>$site_id, 'paginaId'=>$pagina_id));
    $conteudo  = '';
    
    if($blogClass){

        $conteudo  = '';
        if($indice == 1){
            $conteudo  = '';
            $conteudo .= '<div><h1>Categorias</h1></div>';

            $categorias = $blogClass->getCategory();

            if($categorias){
                $conteudo .= '<ul>';
                $conteudo .= '<li><a href="'. base_url('/'.$nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id,true) .'">Todas</a></li>';
                foreach($categorias as $cat){
                    $conteudo .= '<li><a href="'. base_url('/'.$nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id.'&tipo=categoria&categoria='.$cat['id'],true) .'">'. $cat['category'] .'</a></li>';
                }
                $conteudo .= '</ul>';
           }
        }

        if($indice == 2){

            if($blog_acao['tipo'] == 'post' ){

                $post_id = (!empty($blog_acao['post_id']))? $blog_acao['post_id']: '0';
                $post    = $blogClass->getPosts(0,null,$post_id);

                if($post){
                    //Classe que informa se o H2 está com ou sem imagem
                    $class_h2 = (!empty($post['imagem_destaque']))? 'com_imagem' : $class_h2 = 'sem_imagem';

                    $conteudo .= '<div class="edit title" >';
                    $conteudo .= '<h2 class="'. $class_h2  .'" >'. $post[0]['title'] .'</h2>';
                    $conteudo .= '</div>';
                    if(!empty($post['imagem_destaque'])){
                        $post[0]['imagem_destaque'] = str_replace('[local]',base_url('/images/thumb',true),$post[0]['imagem_destaque']);
                        $conteudo .= '<img src="'. $post[0]['imagem_destaque'] .'" class="imagem_destaque">';
                    }
                    $conteudo .= '<div class="post_texto">';
                    $conteudo .= '<div class="data1">'. dataExtenso($post[0]['date_post']) .'</div>';
                    $conteudo .= '<div class="data2">'. dataExtenso($post[0]['date_post'],2) .'</div>';
                    $conteudo .= '</div>';

                    $post[0]['post'] = $blogClass->converteVideo($post[0]['post']);
                    $post[0]['post'] = $blogClass->converteImagem($post[0]['post'],base_url('/images/',true));

                    $conteudo .= $post[0]['post'];
                    //$conteudo .= '<a href="javascript:history.back()" class="btn_voltar">Voltar</a>';
                    //$conteudo .= '</div>';

                    /*
                    if(!empty($facebookActive)){
                        $conteudo .= '
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
                        fjs.parentNode.insertBefore(js, fjs);
                        }(document, "script", "facebook-jssdk"));</script>
                        <div class="fb-comments" data-href="'. $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"] .'" data-width="580" data-num-posts="10"></div>';
                    }*/
                }
                
            }else{
                //Define numero de registros por pagina
                if(!empty($post_por_pagina) && is_numeric($post_por_pagina) ){ $blogClass->maxRegistros = $post_por_pagina; }
                if($blog_acao['tipo'] == 'paginacao' ){
                    $paginacao = (!empty($blog_acao['paginacao']))? $blog_acao['paginacao']: '1';
                    $url_base  = $nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id.'&tipo=paginacao&paginacao=';

                    $posts       = $blogClass->getPosts($paginacao,$categoria_id,null,null,null,true);
                    $total_posts = $blogClass->registros_post;
                }elseif($blog_acao['tipo'] == 'categoria'){
                    $cat_id    = $blog_acao['categoria'];    
                    $paginacao = (!empty($blog_acao['paginacao']))? $blog_acao['paginacao']: '1';
                    $url_base  = $nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id.'&tipo=categoria&categoria='.$cat_id.'&paginacao=';

                    $posts       = $blogClass->getPosts($paginacao,$cat_id);
                    $total_posts = $blogClass->getTotalPosts('where category_id='.$cat_id);
                }


                $total_pages = ceil($total_posts / $blogClass->maxRegistros);
                if($paginacao == 1  && $total_pages > 1){
                    $prev = '';
                    $next = '<a href="'. base_url('/'.$url_base . ($paginacao + 1),true) .'">Próximo</a>';
                }elseif($paginacao > 1 && $total_pages > $paginacao ){
                    $prev = '<a href="'. base_url('/'.$url_base . ($paginacao - 1),true) .'">Anterior</a>';
                    $next = '<a href="'. base_url('/'.$url_base . ($paginacao + 1),true) .'">Próximo</a>';
                }elseif($paginacao > 1 && $total_pages >= $paginacao ){
                    $prev = '<a href="'. base_url('/'.$url_base . ($paginacao - 1),true) .'">Anterior</a>';
                    $next = '';
                }else{
                    $prev = '';
                    $next = '';
                }

                $conteudo  = '';
                //$conteudo .= '<div class="cx_posts">';
                if($posts){
                    $count = 0;
                    foreach($posts as $post){

                        //Classe que informa se o H2 está com ou sem imagem
                        $class_h2 = (!empty($post['imagem_destaque']))? 'com_imagem' : $class_h2 = 'sem_imagem';
                        
                        if($count == 0){
                            $class_first_post = "first_post";
                            $count++;
                        }else{
                            $class_first_post = "";
                        }
                        
                        $conteudo .= '<div class="post '. $class_first_post .' ">';
                        $conteudo .= '<h2 class="'. $class_h2 .'" ><a href="'. base_url('/'.$nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id.'&tipo=post&post_id='.$post['id'],true) .'">'.$post['title'].'</a></h2>';
                        if(!empty($post['imagem_destaque'])){
                            $post['imagem_destaque'] = str_replace('[local]',base_url('/images/thumb',true),$post['imagem_destaque']);
                            $conteudo .= '<div class="cx_imagem_destaque"><img src="'. $post['imagem_destaque'] .'" class="imagem_destaque"></div>';
                        }
                        $conteudo .= '<div class="data1">'. dataExtenso($post['date_post']) .'</div>';
                        $conteudo .= '<div class="data2">'. dataExtenso($post['date_post'],2) .'</div>';


                        $conteudo .= '<div class="post_summary">'.$post['summary'].'</div>';
                        $conteudo .= '<a href="'. base_url('/'.$nomePagina.'?site_id='.$site_id.'&pagina_id='.$pagina_id.'&tipo=post&post_id='.$post['id'],true) .'" class="btn_leiamais">Leia mais</a>';
                        $conteudo .= '</div>';
                        
                    }

                    $conteudo .= '<div class="navBlog">';
                    $conteudo .= '<div class="prev">'. $prev .'</div>';
                    $conteudo .= '<div class="next">'. $next .'</div>';
                    $conteudo .= '</div>';
                    
                }
                //$conteudo .= '</div>';
            }

        }
    }
    
    return $conteudo;
    
}
?>
