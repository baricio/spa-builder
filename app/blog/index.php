<?php
date_default_timezone_set('America/Sao_Paulo');

require '../vendor/autoload.php';

use blog\Libraries\BlogModel;
use Carbon\Carbon;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

Carbon::setLocale('pt_BR');

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
    ],
]);

$app->add(function ($request, $response, $next) {
    $headers = $request->getHeaders();
    $paginaExist = array_key_exists('HTTP_PAGINA_ID', $headers);
    $siteExist = array_key_exists('HTTP_SITE_ID', $headers);
    if ($paginaExist && $siteExist) {
        $response = $next($request, $response);
    } else {
        $response->withStatus(406)
            ->withHeader('Content-Type', 'text/html')
            ->write('Invalid Header params');

    }
    return $response;
});

$app->get('/categoria/list', function (Request $request, Response $response) {

    $headers = $request->getHeaders();
    $blogClass = new BlogModel(array('siteId' => $headers['HTTP_SITE_ID'][0], 'paginaId' => $headers['HTTP_PAGINA_ID'][0]));
    $categorias = $blogClass->getCategory();
    return $response->withJson($categorias);
});

$app->get('/post/list', function (Request $request, Response $response) {
    $headers = $request->getHeaders();
    $blogClass = new BlogModel(array('siteId' => $headers['HTTP_SITE_ID'][0], 'paginaId' => $headers['HTTP_PAGINA_ID'][0]));
    $paginacao = $request->getQueryParam('paginacao', 1);
    $categoria_id = $request->getQueryParam('categoria_id', null);
    $categoria_id = (!is_numeric($categoria_id)) ? null : $categoria_id;

    $posts = array('data' => null);
    $posts['data'] = $blogClass->getPosts($paginacao, $categoria_id, null, null, null, true);

    if (is_array($posts)) {
        foreach ($posts['data'] as &$post) {
            $post['imagem_destaque'] = str_replace('[local]', 'images/thumb', $post['imagem_destaque']);
            $dateCb = Carbon::createFromFormat('Y-m-d', $post['date_post']);
            $post['date1'] = $dateCb->format('d M Y');
            $post['date2'] = $dateCb->format('d \d\e F \d\e Y');
        }
        $posts['total_posts'] = $blogClass->registros_post;
        $posts['total_pages'] = ceil($posts['total_posts'] / $blogClass->maxRegistros);
        $posts['current_page'] = $paginacao;
    }
    return $response->withJson($posts);
});

$app->get('/post/{id}', function (Request $request, Response $response) {
    $headers = $request->getHeaders();
    $blogClass = new BlogModel(array('siteId' => $headers['HTTP_SITE_ID'][0], 'paginaId' => $headers['HTTP_PAGINA_ID'][0]));
    $post_id = $request->getAttribute('id', 0);
    $post = $blogClass->getPosts(0, null, $post_id);
    return $response->withJson($post);
});
$app->run();

