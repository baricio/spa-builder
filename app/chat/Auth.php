<?php

namespace builder\libraries\chat;
use builder\libraries\chat\entity\Config;
use builder\libraries\chat\entity\Response;

/**
 * Class Auth
 * Autorizacao para acesso a API
 * @package builder\libraries\chat
 */
class Auth{

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Cria o usuario admin
     * @return Response
     * @throws \Exception
     */
    public function createAdmin(){

        \Httpful\Request::ini(Template::POST());
        try{
            $response = \Httpful\Request::post($this->config->getUrl() . '/' . $this->config->getRouteCreateadmin())
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * Pega autorização para o admin
     * @return Response
     * @throws \Exception
     */
    public function autorizaAdmin(){

        try{
           return $this->autoriza($this->config->getUser(),$this->config->getPass());
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * Pega autorização para o usuario comum
     * @param $site
     * @param $pass
     * @return Response
     * @throws \Exception
     */
    public function autoriza($site,$pass){

        \Httpful\Request::ini(Template::POST());
        try{
            $response = \Httpful\Request::post($this->config->getUrl() .'/'. $this->config->getRouteAuth())
                ->body(array('site'=>$site,'senha'=>$pass))
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * Recupera o token a partir da resposta
     * @param Response $response
     * @return string
     */
    public function getToken(Response $response){

        $header = $response->getHeaders();

        if(isset($header['x-access-token'])){
            return $header['x-access-token'];
        }

        return '';

    }

}