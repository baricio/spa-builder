<?php
/**
 * Created by PhpStorm.
 * User: Fabricio
 * Date: 15/07/2016
 * Time: 16:30
 */

namespace builder\libraries\chat;

use builder\libraries\chat\entity\Config;
use builder\libraries\chat\entity\Response;

class Chat
{

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function user($site,$token){

        \Httpful\Request::ini(Template::GET($token));
        try{
            $response = \Httpful\Request::get($this->config->getUrlChat().'/'. $site)
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    public function admin($site,$token){

        \Httpful\Request::ini(Template::GET($token));
        try{
            $response = \Httpful\Request::get($this->config->getUrlChatAdmin() .'/'. $site)
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }
}