<?php
/**
 * Created by PhpStorm.
 * User: Fabricio
 * Date: 18/07/2016
 * Time: 14:07
 */

namespace builder\libraries\chat;

use builder\libraries\chat\entity\Config;

require_once 'httpful/bootstrap.php';
require_once "Auth.php";
require_once "FactoryConfig.php";
require_once "Template.php";
require_once 'entity/Config.php';
require_once "entity/Response.php";


class ChatModel
{

    public function getParamsLive(){
        return $this->getParams(FactoryConfig::ConfigChatLive());
    }

    public function getParamsChat(){
        return $this->getParams(FactoryConfig::ConfigChat());
    }

    private function getParams(Config $config){

        $retorno = new \StdClass();
        try{
            $auth = new Auth($config);
            $response = $auth->autoriza($config->getUser(),$config->getPass());

            $token = $auth->getToken($response);

            $retorno->success = true;
            $retorno->url = $config->getUrlChat();
            $retorno->token = $token;
        }catch (\Exception $e){
            $retorno->success = false;
            $retorno->error = $e->getMessage();
            $retorno->data = array();
        }

        return $retorno;

    }

}