<?php
/**
 * Created by PhpStorm.
 * User: Fabricio
 * Date: 11/08/2016
 * Time: 08:54
 */

namespace builder\libraries\chat;
use builder\libraries\chat\entity\Config;

class FactoryConfig
{

    public static function ConfigChat(){
        $config = new Config();
        $config
            ->setUrl('http://chat.hstbr.net')
            ->setUrlChat('http://chat.hstbr.net/chat')
            ->setUrlChatAdmin('http://chat.hstbr.net/chat/admin')
            ->setUser('activesolucoes.com')
            ->setPass('zY9k7SfZpR')
            ->setRouteCreateadmin('createadmin')
            ->setRouteAuth('autenticar')
            ->setRouteUser('user');

        return $config;
    }

    public static function ConfigChatLive(){
        $config = new Config();
        $config
            ->setUrl('http://chatlive.hstbr.net')
            ->setUrlChat('http://chatlive.hstbr.net/chat')
            ->setUrlChatAdmin('http://chatlive.hstbr.net/chat/admin')
            ->setUser('activesolucoes.com')
            ->setPass('wQ5u1DKrTH')
            ->setRouteCreateadmin('createadmin')
            ->setRouteAuth('autenticar')
            ->setRouteUser('user');

        return $config;
    }

}