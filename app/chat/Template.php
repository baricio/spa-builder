<?php

namespace builder\libraries\chat;

use Httpful\Http;
use Httpful\Mime;

/**
 * Class Template
 * Template para as respostas HTTP
 * @package builder\libraries\chat
 */
class Template{

    /**
     * Template GET
     * @param string $token
     * @return mixed
     */
    public static  function GET($token = ''){
        $template =  \Httpful\Request::init()
            ->method(Http::GET)        // Alternative to Request::post
            ->withoutStrictSsl()        // Ease up on some of the SSL checks
            ->expectsHtml();            // Expect JSON responses

        if(!empty($token)){
            $template->addHeader('x-access-token',$token);
        }

        return $template;
    }

    /**
     * Template POST
     * @param string $token
     * @return mixed
     */
    public static  function POST($token = ''){
        $template =  \Httpful\Request::init()
            ->method(Http::POST)        // Alternative to Request::post
            ->withoutStrictSsl()        // Ease up on some of the SSL checks
            ->expectsHtml()             // Expect JSON responses
            ->sendsType(Mime::FORM);    // Send application/x-www-form-urlencoded

        if(!empty($token)){
            $template->addHeader('x-access-token',$token);
        }

        return $template;
    }

    /**
     * Template DELETE
     * @param string $token
     * @return mixed
     */
    public static  function DELETE($token = ''){
        $template =  \Httpful\Request::init()
            ->method(Http::DELETE)        // Alternative to Request::post
            ->withoutStrictSsl()        // Ease up on some of the SSL checks
            ->expectsHtml()             // Expect JSON responses
            ->sendsType(Mime::FORM);    // Send application/x-www-form-urlencoded

        if(!empty($token)){
            $template->addHeader('x-access-token',$token);
        }

        return $template;
    }



}