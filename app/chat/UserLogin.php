<?php

namespace builder\libraries\chat;

use builder\libraries\chat\entity\Response;
use builder\libraries\chat\entity\Config;

/**
 * Class UserLogin
 * Acessa api referente aos usuarios do chat
 * @package builder\libraries\chat
 */
class UserLogin{

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * encontra o usuario pelo site
     * @param $site string
     * @param $token string
     * @return Response
     * @throws \Exception
     */
    public function find($site,$token){

        \Httpful\Request::ini(Template::GET($token));
        try{
            $response = \Httpful\Request::get($this->config->getUrl() . '/' . $this->config->getRouteUser() .'/'. $site)
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * cadastra um novo usuario ou atualiza caso exista
     * @param $site
     * @param $senha
     * @param $token
     * @return Response
     * @throws \Exception
     */
    public function create($site,$senha,$token){

        \Httpful\Request::ini(Template::POST($token));
        try{
            $response = \Httpful\Request::post($this->config->getUrl() . '/' .$this->config->getRouteUser())
                ->body(array('site'=>$site,'senha'=>$senha))
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * Remove usuário pela referência do site
     * @param $site
     * @param $token
     * @return Response
     * @throws \Exception
     */
    public function remove($site,$token){

        \Httpful\Request::ini(Template::DELETE($token));
        try{
            $response = \Httpful\Request::delete($this->config->getUrl() . '/' . $this->config->getRouteUser())
                ->body(array('site'=>$site))
                ->send();

            return new Response($response->code, $response->body, $response->headers);
        }catch (\Httpful\Exception\ConnectionErrorException $e){
            throw new \Exception($e->getMessage());
        }

    }

}