<?php

namespace builder\libraries\chat\entity;

/**
 * Class Config
 * Configurações das variáveis
 * @package builder\libraries\chat
 */
class Config{

    private  $url;
    private  $url_chat;
    private  $url_chat_admin;
    private  $user;
    private  $pass;
    private  $route_createadmin;
    private  $route_auth;
    private  $route_user;

    /**
     * @param mixed $url
     * @return Config
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param mixed $url_chat
     * @return Config
     */
    public function setUrlChat($url_chat)
    {
        $this->url_chat = $url_chat;
        return $this;
    }

    /**
     * @param mixed $url_chat_admin
     * @return Config
     */
    public function setUrlChatAdmin($url_chat_admin)
    {
        $this->url_chat_admin = $url_chat_admin;
        return $this;
    }

    /**
     * @param mixed $user
     * @return Config
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param mixed $pass
     * @return Config
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
        return $this;
    }

    /**
     * @param mixed $route_createadmin
     * @return Config
     */
    public function setRouteCreateadmin($route_createadmin)
    {
        $this->route_createadmin = $route_createadmin;
        return $this;
    }

    /**
     * @param mixed $route_auth
     * @return Config
     */
    public function setRouteAuth($route_auth)
    {
        $this->route_auth = $route_auth;
        return $this;
    }

    /**
     * @param mixed $route_user
     * @return Config
     */
    public function setRouteUser($route_user)
    {
        $this->route_user = $route_user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getUrlChat()
    {
        return $this->url_chat;
    }

    /**
     * @return mixed
     */
    public function getUrlChatAdmin()
    {
        return $this->url_chat_admin;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return mixed
     */
    public function getRouteCreateadmin()
    {
        return $this->route_createadmin;
    }

    /**
     * @return mixed
     */
    public function getRouteAuth()
    {
        return $this->route_auth;
    }

    /**
     * @return mixed
     */
    public function getRouteUser()
    {
        return $this->route_user;
    }


    /*
    public static $URL = 'http://chat.hstbr.net:3300';
    public static $URL_CHAT = 'http://chat.hstbr.net:3300/chat';
    public static $URL_CHAT_ADMIN = 'http://chat.hstbr.net:3300/chat/admin';
    public static $USER = 'builderadmin';
    public static $PASS = 'SkdeQW#$%2!012938M';
    public static $ROUTE_CREATEADMIN = 'createadmin';
    public static $ROUTE_AUTH = 'autenticar';
    public static $ROUTE_USER = 'user';
    */
    
    

}