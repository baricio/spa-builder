<?php

namespace builder\libraries\chat\entity;

/**
 * Class Response
 * guarda e retorna as repostas da API do chat
 * @package builder\libraries\chat\entity
 */
class Response{

    private $status;
    private $result;
    private $headers;

    /**
     * Carrega dados da resposta
     * @param $status
     * @param $result
     * @param \Httpful\Response\Headers $headers
     */
    public function __construct($status, $result, \Httpful\Response\Headers $headers)
    {
        $this->status = $status;
        $this->result = $result;
        $this->headers = $headers;
    }

    /**
     * Retorn as resposta em forma de objeto
     * @return mixed
     */
    public function toObject(){
        return json_decode($this->result);
    }

    /**
     * Retorna a resposta em array
     * @return mixed
     */
    public function toArray(){
        return json_decode($this->result,true);
    }

    /**
     * Verifia se o status está ok
     * @return bool
     */
    public function ok(){
        return ($this->status == 200 || $this->status == 204);
    }

    /**
     * Ferifica se não autorizado
     * @return bool
     */
    public function notAuth(){
        return ($this->status == 401);
    }

    /**
     * Status não encontrado
     * @return bool
     */
    public function notFound(){
        return ($this->status == 404);
    }

    /**
     * Stattus erro
     * @return bool
     */
    public function error(){
        return ($this->status == 500);
    }

    /**
     * verifica status
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * pega retorno da resposta
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }


    /**
     * recupera cabecalho
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }


}