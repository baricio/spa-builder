<?php
date_default_timezone_set('America/Sao_Paulo');
require '../vendor/autoload.php';

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
    ],
]);

$app->get('/live', function (Request $request, Response $response) {
    $chat = new \builder\libraries\chat\ChatModel();
    $dadosLive = $chat->getParamsLive();
    return $response->withJson($dadosLive);
});

$app->get('/site', function (Request $request, Response $response) {
    $chat = new \builder\libraries\chat\ChatModel();
    $dadosChat = $chat->getParamsChat();
    return $response->withJson($dadosChat);
});

$app->run();
