'use strict';

angular.module('myApp.banner', [])

.directive('banner', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        templateUrl: "/views/partial/banner.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            $(document).ready(function(){
    
              var intervalo_save_background;
              
              $(".rslides").responsiveSlides({
                  auto:  true,
                  pager: false,
                  nav:   true,
                  pause: true,
                  speed: 500,
                  maxwidth: 1200,
                  namespace: "slider"
              });
              
              $('.topdown .rslidesCarrosel').each(function(i,val){
                  newSlidesExport(val);
                  removeResizableSlider();
              });
              
              $('#menu_carrossel .btn_remover_background').click(function(){
                  $('.edit.carrossel.selected .content').css('background-color','');
                  $('.edit.carrossel.selected').find('input[name=background]').val('');
                  $('#menu_carrossel .cor').css('background-color','');
                  var save = new saveMe($('.edit.carrossel.selected'));
                  save.verify($('.edit.carrossel.selected').attr('tipo'));
              });
              
              if($("#menu_carrossel .cor").length > 0){
                  $("#menu_carrossel .cor").spectrum({
                      color: $('.edit.carrossel.selected .content').css('background-color'),
                      preferredFormat: "hex",
                      showInput: true,
                      showButtons: true,
                      beforeShow: function(){
                          $("#menu_carrossel .cor").css('background',$('.edit.carrossel.selected .content').css('background-color'));
                      },
                      move: function(color){
                          $("#menu_carrossel .cor").css('background',color.toHexString());
                          $('.edit.carrossel.selected .content').css('background-color',color.toHexString());
                          $('.edit.carrossel.selected').find('input[name=background]').val(color.toHexString());

                          clearInterval(intervalo_save_background);
                          intervalo_save_background = setInterval(function(){
                              var save = new saveMe($('.edit.carrossel.selected'));
                              save.verify($('.edit.carrossel.selected').attr('tipo'));
                              
                              clearInterval(intervalo_save_background);
                              intervalo_save_background = null;
                          },500);
                      },
                      hide: function(color){
                          $("#menu_carrossel .cor").css('background',color.toHexString());
                          $('.edit.carrossel.selected .content').css('background-color',color.toHexString());
                          $('.edit.carrossel.selected').find('input[name=background]').val(color.toHexString());
                          var save = new saveMe($('.edit.carrossel.selected'));
                          save.verify($('.edit.carrossel.selected').attr('tipo'));
                          
                          clearInterval(intervalo_save_background);
                          intervalo_save_background = null;
                      },
                      change : function(color){
                          $("#menu_carrossel .cor").css('background',color.toHexString());
                          $('.edit.carrossel.selected .content').css('background-color',color.toHexString());
                          $('.edit.carrossel.selected').find('input[name=background]').val(color.toHexString());
                          var save = new saveMe($('.edit.carrossel.selected'));
                          save.verify($('.edit.carrossel.selected').attr('tipo'));
                          
                          clearInterval(intervalo_save_background);
                          intervalo_save_background = null;
                      }
                  });
              }
          });
        }]
    }
});