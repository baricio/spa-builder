'use strict';

angular.module('myApp.blog.controller', [])

.controller('blogCtrl', [
    '$scope','$state','$stateParams','categoria','post', 
    function($scope, $state, $stateParams, categoria, post){

    $scope.siteId = $stateParams.siteId;
    $scope.pageId = $stateParams.pageId;
    $scope.postId = $stateParams.postId;
    $scope.categoriaId = $stateParams.categoriaId;
    $scope.paginate = $stateParams.paginate;
    $scope.isLastPage = false;

    console.log('$stateParams',$stateParams);
    $scope.post = {};

    var error = function(e) {
        console.log('request fail', e);
    }

    var getDefaultUrl = function() {
        var url = '#!/'+$state.$current.name +'/';
        url += $scope.siteId + '/' + $scope.pageId
        return url;
    }

    var checkLastPage = function() {
        $scope.isLastPage = parseInt($scope.posts.current_page,10) == Math.ceil($scope.posts.total_posts / 10);
        console.log('isLastPage',$scope.isLastPage);
    }

    categoria.getCategorias($scope.siteId,$scope.pageId)
    .success(function(data){
      $scope.categorias = data;
    }).error(error);

    if ($scope.postId) {
        post.getPost($scope.siteId, $scope.pageId, $scope.postId)
        .success(function(data){
          $scope.post = data[0];
        }).error(error);
    } else {
        post.getPosts($scope.siteId,$scope.pageId, $scope.categoriaId, $scope.paginate)
        .success(function(data){
          $scope.posts = data;
          checkLastPage();
        }).error(error);
    }

    $scope.getUrl = function(categoria_id){
        return getDefaultUrl();
    };

    $scope.getCategoryUrl = function(categoria_id){
        return getDefaultUrl() + '?categoriaId=' + categoria_id
    };

    $scope.getPostUrl = function(post_id){
        return getDefaultUrl() + '?postId=' + post_id
    };

    $scope.getPaginatePrev = function(paginate){
        var page = parseInt(paginate,10) - 1;
        var categoria_id = (typeof $scope.categoriaId !== 'undefined') ? $scope.categoriaId : '';
        return getDefaultUrl() + '?paginate=' + page + '&categoriaId=' + categoria_id;
    };

    $scope.getPaginateNext = function(paginate){
        var page = parseInt(paginate,10) + 1;
        var categoria_id = (typeof $scope.categoriaId !== 'undefined') ? $scope.categoriaId : '';
        return getDefaultUrl() + '?paginate=' + page + '&categoriaId=' + categoria_id;
    };

}]);