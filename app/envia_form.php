﻿<?php
include_once 'function_captcha.php';
include_once 'phpmail/class.phpmailer.php';

$data = array('msg' => 'Não permitido');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $c_form = isset($_POST['cap']) ? trim($_POST['cap']) : '';
    $c_session = isset($_SESSION['scnumber']) ? trim($_SESSION['scnumber']) : '';

    if (($c_form !== '' && $c_session !== '') && ($c_form === $c_session)) {
        // envia
        $conteudo = '<div style="font-size: 22px;border-bottom: 2px solid #ff0000;color: rgb(65, 65, 95);">';
        $conteudo .= 'Email enviado pelo sistema Max';
        $conteudo .= '</div>';
        foreach ($_POST['form'] as $key => $value) {

            if (key($value) == 'title') {
                $conteudo .= '<h1>' . $key . '</h1>';
            } elseif (key($value) == 'text') {
                $conteudo .= '<label style="float:left"><strong>' . $key . ':</strong> </label><br />';
                $conteudo .= '<span style="float:left">' . $value[key($value)] . '</span>';
            } elseif (strpos(key($value), 'checkbox') !== false) {
                $conteudo .= '<label style="float:left"><strong>' . $key . ':</strong> </label><br />';
                $conteudo .= '<span style="float:left">' . implode($value[key($value)]) . '</span>';
            } elseif (strpos(key($value), 'radio') !== false) {
                $conteudo .= '<label style="float:left"><strong>' . $key . ':</strong> </label><br />';
                $conteudo .= '<span style="float:left">' . $value[key($value)] . '</span>';
            } elseif (key($value) == 'select') {
                $conteudo .= '<label style="float:left"><strong>' . $key . ':</strong> </label><br />';
                $conteudo .= '<span style="float:left">' . $value[key($value)] . '</span>';
            } elseif (key($value) == 'textarea') {
                $conteudo .= '<label style="float:left"><strong>' . $key . ':</strong> </label><br />';
                $conteudo .= '<span style="float:left">' . $value[key($value)] . '</span>';
            }
            $conteudo .= '<br /><br />';
        }

        $emailto = "baricio@gmail.com";
        $msgEnvio = (isset($_POST['mensagem'])) ? $_POST['mensagem'] : "Mensagem enviada com sucesso";

        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->SMTPAuth = true; // enable SMTP authentication
        //$mail->SMTPSecure = "ssl";       // sets the prefix to the servier
        $mail->Host = "mail.dev.ipnv.org.br"; // sets the SMTP server
        $mail->Port = "587"; // set the SMTP port

        $mail->Username = "contato@dev.ipnv.org.br"; // username
        $mail->Password = "ivoldAHo"; // password

        $mail->AddReplyTo('contato@dev.ipnv.org.br', 'contato@dev.ipnv.org.br');
        $mail->From = "contato@dev.ipnv.org.br";
        $mail->FromName = "Contato do site";
        $mail->Subject = "Mensagem de contato";
        //$mail->AltBody    = "This is the body when user views in plain text format"; //Text Body
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50; // set word wrap

        $mail->AddAddress($emailto, "User");

        $mail->MsgHTML($conteudo);

        $mail->IsHTML(true); // send as HTML

        $mail->SMTPDebug = 0;

        if (!$mail->Send()) {
            $data = array("msg" => $mail->ErrorInfo, "error" => 1, "captcha" => 1);
        } else {
            $data = array("msg" => $msgEnvio, "error" => 0, "captcha" => 1);
        }

    } else {
        $data = array("msg" => "captcha incorreto", "error" => 0, "captcha" => 0);
    }
}

create_sequence();
echo json_encode($data);
