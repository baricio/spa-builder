'use strict';

angular.module('myApp.blog.factory', [])

.factory('categoria', function($http){
    return {
        getCategorias: function(siteId, pageId) {
          return $http({
              method: 'GET',
              url: '/blog/categoria/list',
              headers: {
                  'site_id': siteId,'pagina_id':pageId
              }
          });
        }
    };
})

.factory('post', function($http){
    return {
        getPosts: function(siteId, pageId, categoriaId, paginacao) {
           categoriaId = (typeof categoriaId !== 'undefined') ? categoriaId : '';
           paginacao = (typeof paginacao !== 'undefined') ? paginacao : 1;
          return $http({
              method: 'GET',
              url: '/blog/post/list',
              params: {'categoria_id': categoriaId, 'paginacao':paginacao},
              headers: {
                  'site_id': siteId,'pagina_id':pageId
              }
          });
        },
        getPost: function(siteId, pageId, postId) {
          return $http({
              method: 'GET',
              url: '/blog/post/' + postId,
              headers: {
                  'site_id': siteId,'pagina_id':pageId
              }
          });
        }
    };
});

