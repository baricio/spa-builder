'use strict';

angular.module('myApp.chat.factory', [])

.factory('chat', function($http){
    return {
        getLive: function() {
          return $http({
              method: 'GET',
              url: '/chat/live',
          });
        },
        getSite: function() {
          return $http({
              method: 'GET',
              url: '/chat/site',
          });
        }
    };
})
