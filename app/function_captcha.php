<?php
session_start();

function ImageCaptcha()
{

    create_sequence();

    $img = imagecreatefromjpeg("images/texture.jpg");

    /*texto da imagem*/
    $text_pass = $_SESSION['scnumber'];
    $security_number = empty($text_pass) ? 'error' : $text_pass;
    $image_text = $security_number;

    /*cores rand�micas para as letras*/
    $red = rand(100, 255);
    $green = rand(100, 255);
    $blue = rand(100, 255);

    /*criando a cor das letras*/
    $text_color = imagecolorallocate($img, 255 - $red, 255 - $green, 255 - $blue);

    /*adiciona o texto guardado em $image_text*/
    $font = "fonts/courbd.ttf";
    $text = imagettftext($img, 16, rand(-10, 10), rand(10, 20), rand(25, 35), $text_color, $font, $image_text);
    //die();
    header("Content-type:image/jpeg");
    header("Content-Disposition:inline ; filename=secure.jpg");
    imagejpeg($img);

}

function create_sequence()
{

    $cap = array();
    $cap['length'] = 6;
    $cap['characters'] = str_split('23456789abcdefghijkmnpqrstuvxyzw');
    $cap['string'] = "";

    while (strlen($cap['string']) < $cap['length']) {
        $randomChar = mt_rand(0, count($cap['characters']) - 1);
        $cap['string'] .= $cap['characters'][$randomChar];
    }

    $_SESSION['scnumber'] = $cap['string'];
}
