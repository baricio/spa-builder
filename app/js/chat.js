//widget espaçamento 
var widgetChat = {
    
    init: function( settings ) {
        widgetChat.config = {
            content: $('body'),
            site_id: $('#site').val(),
            url: '',
            token: ''
        };
 
        // Allow overriding the default config
        $.extend( widgetChat.config, settings );
 
        return widgetChat.setup();
    },
 
    setup: function() {
        return this;
    },

    createChat: function(){
        widgetChat.create(".max-chat");
    },

    createLive: function(){
        widgetChat.create(".max-chat-live");
    },

    create: function(selector){
        if(widgetChat.config.token) {
            var elementChat = document.querySelectorAll(selector);
            for (var i = 0; i < elementChat.length; ++i) {
                var ifrm = document.createElement("iframe");
                ifrm.setAttribute("src", widgetChat.config.url +'?token='+ encodeURI(widgetChat.config.token) );
                ifrm.style.width  = "100%";
                ifrm.style.height = "100%";
                ifrm.frameBorder  = "0";
                elementChat[i].parentNode.replaceChild(ifrm,elementChat[i]);
            }
        }
    },

    enable : function(){
        widgetChat.disable();

        //adiciona atributo informando que a tabela precisa ser salva
        $('.edit.chat.selected').attr('noSave','1');

        //habilita o resize
        $('.edit.chat.selected .content').resizable({handles: 's'});
    },
    
    disable : function(){
        $('.edit.chat .content').each(function(){
            if($(this).data('ui-resizable')){
                $(this).resizable('destroy');
            }
        });

        modelWidgets.init({elementSave:$('.edit.chat[noSave]')}).save();
        $('.edit.chat[noSave]').removeAttr('noSave');
    }
};
