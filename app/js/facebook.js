/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//CreateNewLikeButton(document.URL,$('.container'));
function CreateNewLikeButton(url,element,options)
{
    var width = '100%';
    if( element.parents('.tag').length == 0 ){
        width = element.parents('.columns').width();
    }else{
        width = element.parents('.tag').width();
    }
 
    if(empty(options)){ options = {}; }        
    if(!('send' in options)){ options['send'] = "true"; }
    if(!('layout' in options)){ options['layout'] = "button_count"; }
    if(!('width' in options)){ options['width'] = width}
    if(!('alterar_conteudo' in options)){ options['alterar_conteudo'] = true}
    
    var eLike = $(document.createElement("fb:like"));
    eLike.attr("class", "intro_like");
    eLike.attr("href", url);
    eLike.attr("send", options.send);
    eLike.attr("data-layout", options.layout);
    eLike.attr("width", options.width );
    
    if($('#site_escuro').val() == 1){
        eLike.attr("data-colorscheme","dark");
    }
    
    if(options.alterar_conteudo){
        element.html(eLike);
    }else{
        element.append(eLike);
    }
    
    FB.XFBML.parse(element[0],notifyRender);
   
}

function CreateNewComents(url,element)
{
    var width = '100%';
    if( element.parents('.tag').length == 0 ){
        width = element.parents('.columns').width();
    }else{
        width = element.parents('.tag').width();
    }
    
    var eCom = $(document.createElement("fb:comments"));
    eCom.attr("href", url);
    eCom.attr("width", width );
    eCom.attr("num_posts", 3);
    if($('#site_escuro').val() == 1){
        eCom.attr("data-colorscheme","dark");
    }
    element.html(eCom);
    FB.XFBML.parse(element[0],notifyRender);
}

function CreateNewLikeBox(url,element)
{
    var width = '100%';
    if( element.parents('.tag').length == 0 ){
        width = element.parents('.columns').width();
    }else{
        width = element.parents('.tag').width();
    }
    
    var eBox = $(document.createElement("fb:page"));
    eBox.attr("href", url);
    eBox.attr("width", width );
    eBox.attr("show_facepile", "true");
    if($('#site_escuro').val() == 1){
        eBox.attr("data-colorscheme","dark");
    }
    element.html(eBox);
    FB.XFBML.parse(element[0],notifyRender);
}

function resizableSocialIcons(element){
    
    var intervalo_save_social_links;
    var min_width = 26; 
    var define_para,max_width;
    
    if($(element).parents('.columns:first').length > 0){//corpo do site
        max_height  = 0;
        max_width   = $(element).width();
        define_para = 'intro';
    }else{
        max_height  = $(element).parents('.topdown').height();
        max_width   = $(element).parents('.topdown').width();
        define_para = 'cabecalho';
    }
    
    $(element).find('.content').resizable({
         handles: "e",
         maxWidth: max_width,
         minWidth: min_width,
         maxHeight: min_width,
         resize:function(){            
                        
            var element_content = $(this);
             
            clearInterval(intervalo_save_social_links);
            intervalo_save_social_links = setInterval(function(){
                if(define_para === 'cabecalho'){
                    ajustePositionTopDown(element_content.parents('.edit.social_links:first'));

                    var save = new saveMe($('#topo'));
                    save.verify($('#topo').attr('id'));

                    save = new saveMe($('#rodape'));
                    save.verify($('#rodape').attr('id'));
                    delete save;
                }else{
                    var save = new saveMe(element_content.parents('.edit.social_links:first'));
                    save.verify(element_content.parents('.edit.social_links:first').attr('tipo'));
                    delete save;
                }
                
                clearInterval(intervalo_save_social_links);
                intervalo_save_social_links = null;
            },500);
             
         },
         stop: function(){
            if(define_para === 'cabecalho'){
                ajustePositionTopDown($(this).parents('.edit.social_links:first'));
                
                var save = new saveMe($('#topo'));
                save.verify($('#topo').attr('id'));

                save = new saveMe($('#rodape'));
                save.verify($('#rodape').attr('id'));
                delete save;
            }else{
                var save = new saveMe($(this).parents('.edit.social_links:first'));
                save.verify($(this).parents('.edit.social_links:first').attr('tipo'));
                delete save;
            }
            
         }
    });
}

function removeResizableSocialIcons(){
    $('.edit.social_links .content').each(function(){
        if($(this).data('ui-resizable')){
            $(this).resizable('destroy');
        }
    });
}

function menuSocialIcons(){
    var edit_content = $('.edit.selected.social_links .content');
    var align        = edit_content.attr('aligntype');
    
    if(align == 'center'){
        $('#menu_social_links input[cmdsl=justifyleft]').removeAttr('class').addClass('alinhado_esq');
        $('#menu_social_links input[cmdsl=justifycenter]').removeAttr('class').addClass('centralizado_ativo');
        $('#menu_social_links input[cmdsl=justifyright]').removeAttr('class').addClass('alinhado_dir');
        
        edit_content.css('float','none');
        edit_content.css('margin','0 auto');
    }else if(align == 'right'){
        $('#menu_social_links input[cmdsl=justifyleft]').removeAttr('class').addClass('alinhado_esq');
        $('#menu_social_links input[cmdsl=justifycenter]').removeAttr('class').addClass('centralizado');
        $('#menu_social_links input[cmdsl=justifyright]').removeAttr('class').addClass('alinhado_dir_ativo');
        
        edit_content.css('float','right');
        edit_content.css('margin','');
    }else if(align == 'left'){
        $('#menu_social_links input[cmdsl=justifyleft]').removeAttr('class').addClass('alinhado_esq_ativo');
        $('#menu_social_links input[cmdsl=justifycenter]').removeAttr('class').addClass('centralizado');
        $('#menu_social_links input[cmdsl=justifyright]').removeAttr('class').addClass('alinhado_dir');
        
        edit_content.css('float','left');
        edit_content.css('margin','');
    }
    
    var save = new saveMe($('.edit.social_links.selected'));
    save.verify($('.edit.social_links.selected').attr('tipo'));
    delete save;
}

function CreateNewCompartTwitter(url, element, texto){

	var tweet = $(document.createElement("a"));
            tweet.attr('class',"intro-twitter");
            tweet.attr('id',"twitter-share-button-new");

            element.append(tweet);

        twttr.ready(function (twttr) {
            twttr.widgets.createShareButton(
                url,
                document.getElementById('twitter-share-button-new'),
                function(el) {
                      //console.log("Button created.")
                },
                {
                      lang : 'pt-br',
                      count: 'horizontal',
                      text : texto
                }
            );
            document.getElementById('twitter-share-button-new').id = 'twitter-share-button';
        });
        
}

function CreateNewCompartPlus(url,element){

        //cria um id unico
	var n	   = Math.floor(Math.random()*11);
	var k 	   = Math.floor(Math.random()* 1000000);
	var uniqid = String.fromCharCode(n)+k;

	var gplus = $(document.createElement("div"));
            gplus.attr('class',"intro-twitter");
            gplus.attr('id',uniqid);
	
	element.append(gplus);
	
	gapi.plusone.render(
		uniqid,
		{href:url,
                 size:'medium'
                }
	);
	
}

function CreateNewCompartLinks(url, element){ 
    if($(element).attr('facebook_active') == 1){
        CreateNewLikeButton(url,element,{'send':'false','layout':'button_count','width':'','alterar_conteudo':false});        
    }
    
    if($(element).attr('twitter_active') == 1){
        CreateNewCompartTwitter(url,element);
    }
    
    if($(element).attr('plus_active') == 1){
        CreateNewCompartPlus(url,element);
    }
}

$(document).ready(function(){
    
    $('#menu_likebox .addLikebox').click(function(){  
        var url_facebook = $('#menu_likebox').find('input').val();
        //verifica se é uma url valida para o likebox
        $.ajax({
            dataType: "json",
            url: BASE_URL + '/site/validation/facebook_page',
            data: {url:url_facebook},
            type: 'POST',
            success: function(data){
                if(data.success == true ){
                    $('.edit.facebook_likebox.selected').find('.url').val(url_facebook);
                    CreateNewLikeBox(url_facebook,$('.edit.facebook_likebox.selected').find('.content'));
                    var save = new saveMe($('.edit.facebook_likebox.selected'));
                    save.verify($('.edit.facebook_likebox.selected').attr('tipo'));
                    delete save;
                }else{
                    apprise("Pagina do facebook não encontrada.");
                }
            },
            error:function(){
                apprise("Falha ao verificar pagina. Favor tentar novamente");
            }
        });
        
    });
    
    $('#menu_social_links input').click(function(){
        
        var cmdsl   = $(this).attr('cmdsl');
        var imgtype = $('.edit.selected.social_links .content').attr('imgtype');
        
        if(cmdsl === 'justifyright'){
           $('.edit.selected.social_links .content').attr('aligntype','right');
        }else if(cmdsl === 'justifycenter'){
           $('.edit.selected.social_links .content').attr('aligntype','center');
        }else if(cmdsl === 'justifyleft'){
           $('.edit.selected.social_links .content').attr('aligntype','left');
        }else if(cmdsl === 'icon'){
           if($('.cx_icones_sociais').is(':visible')){
               $('.cx_icones_sociais').hide();
               $('#menu_social_links input[cmdsl=icon]').removeClass('ativo');
           }else{
               $('.cx_icones_sociais').show();
               $('#menu_social_links input[cmdsl=icon]').addClass('ativo');
           }
        }
        
        $('.cx_icones_sociais .icone').removeClass('ativo');
        $('.cx_icones_sociais img.' + imgtype).parent().addClass('ativo');
        
        menuSocialIcons();
    });
    
    $('#menu_social_links .cx_icones_sociais .icone').click(function(){
        var classe = $(this).find('img').attr('class');
        
        $('.cx_icones_sociais .icone').removeClass('ativo');
        $(this).addClass('ativo');
        
        $('.edit.selected.social_links .content').attr('imgtype',classe);
        $('.edit.selected.social_links .content').removeAttr('class').addClass('content').addClass(classe);
        
        menuSocialIcons();
    });
});

function notifyRender(){
    if(typeof notificaFacebookRender == 'function'){
        notificaFacebookRender();
    }
}