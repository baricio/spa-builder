var imagemMobile = {

    init: function(settings){
        imagemMobile.css = {
            logo: ''
        }

        imagemMobile.config = {};

        imagemMobile.config.topdown = {
            logo: $('.topdown .logo_img_titulo'),
            resize: $('.topdown .logo_img_titulo .resize'),
            imgDrag: $('.topdown .logo_img_titulo .img_drag'),
            image: $('.topdown .logo_img_titulo .img_drag img'),
        }

        imagemMobile.config.site = {
            cxImage: $('.edit .cx_imagem'),
            resize: $('.edit .cx_imagem .resize'),
            imgDrag: $('.edit .cx_imagem .img_drag'),
            image: $('.edit .cx_imagem .img_drag img'),
        }

        imagemMobile.config.banner = {
            cxBanner: $('.edit.carrossel .content')
        }

        imagemMobile.config.video = {
            cxVideo: $('.edit.video .content')
        }

        $.extend(menuMobile.config, settings);

        imagemMobile.setup();
    },

    setup: function(){
        imagemMobile.removeCss();
        //imagemMobile.ajustaImagem();
    },

    removeCssLogo: function(){
        imagemMobile.config.topdown.logo.attr('style','');
        imagemMobile.config.topdown.resize.attr('style','');
        imagemMobile.config.topdown.imgDrag.attr('style','');
        imagemMobile.config.topdown.image.attr('style','');
    },

    removeCssSite: function(){
        imagemMobile.config.site.cxImage.attr('style','');
        imagemMobile.config.site.resize.attr('style','');
        imagemMobile.config.site.imgDrag.attr('style','');
        imagemMobile.config.site.image.attr('style','');
    },

    removeCssBanner: function(){
        imagemMobile.config.banner.cxBanner.attr('style','');
    },

    removeCssVideo: function(){
        imagemMobile.config.video.cxVideo.attr('style','');
    },

    ajustaImagensSite: function(){
        imagemMobile.config.site.image.css('width','100%');
        imagemMobile.config.site.image.css('height','auto');
    },

    ajustaLogo: function(){
        imagemMobile.config.topdown.image.css('width','100%');
        imagemMobile.config.topdown.image.css('height','auto');
    },

    ajustaBanner: function(){
        imagemMobile.config.banner.cxBanner.css('width','100%');
    },

    removeCss: function(){
        imagemMobile.removeCssLogo();
        imagemMobile.removeCssSite();
        imagemMobile.removeCssVideo();
        imagemMobile.removeCssBanner();
    },

    ajustaImagem: function(){
        imagemMobile.ajustaImagensSite();
        imagemMobile.ajustaLogo();
        imagemMobile.ajustaBanner();
    }

}
