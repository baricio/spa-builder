var geocoder;
var map;
var marker; 
var map_endereco = '';
 
function initialize(){
    
    var element;
    
    $('.local').each(function(){
        
        element = $(this);
        
        var latitude         = parseFloat(element.find('input[name=latitude]').val());
        var longitude        = parseFloat(element.find('input[name=longitude]').val());
        var center_latitude  = parseFloat(element.find('input[name=center_latitude]').val());
        var center_longitude = parseFloat(element.find('input[name=center_longitude]').val());

        var latlng = new google.maps.LatLng(center_latitude, center_longitude);
        var options = {
            zoom: parseInt(element.find('input[name=zoom]').val()),
            draggable: true, 
            streetViewControl:true,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map      = new google.maps.Map(element.find('.mapa')[0], options); 
        geocoder = new google.maps.Geocoder();
        marker   = new google.maps.Marker({
            map: map,
            draggable: false,
            disableDoubleClickZoom: true
        });

        var latlngMarker = new google.maps.LatLng(latitude, longitude);
        marker.setPosition(latlngMarker);

        infoMapa  = (element.find('input[name=title]').val() != '')?  '<h1>'+ element.find('input[name=title]').val() +'</h1>' : '';
        infoMapa += element.find('input[name=message]').val(); 
        marker.info = new google.maps.InfoWindow({
            content: '<div contenteditable=false>'+ infoMapa +'</div>'
        });
        marker.info.open(map, marker);

        //event click do marker
        google.maps.event.addListener(marker, 'click', function(){
            marker.info.open(map,marker);
        });

        //ajusta tamanho do mapa na tela
        google.maps.event.trigger(map, 'resize');
    
    });
    
}