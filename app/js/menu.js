var menuMobile = {

    init: function(settings){
        menuMobile.config = {
            hamburguer: $('.menu_hamburguer'),
            menu: $('#sec_menu'),
            menuRow: $('#sec_menu .row'),
            efeito: 'show',
            tempo: 500
        }

        $.extend(menuMobile.config, settings);

        menuMobile.setup();
    },

    setup: function(){
        menuMobile.config.hamburguer.click(menuMobile.toggleMenu);
        menuMobile.config.menuRow.click(menuMobile.toggleMenu);
    },

    toggleMenu: function(){
        menuMobile.config.menu
            .toggle( );
    },

};