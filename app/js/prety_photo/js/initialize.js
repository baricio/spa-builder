/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    addEventGaleria();
});

function addEventGaleria(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
    
    $('.galeria.edit .content').sortable({
         items: ".cx_imagem_galeria"
        ,stop:function(event,ui){
            var save = new saveMe($(ui.item).parents('.edit:first'));
            save.verify($(ui.item).parents('.edit:first').attr('tipo'));
            delete save;
        }
    });
    
    $('.galeria.edit .content').off('mouseenter','a[galeria]').on('mouseenter','a[galeria]',function(){
        $('.galeria.edit .content').find('.btn_excluir').remove();
        $(this).before('<div class="btn_excluir"></div>');
        $(this).parent().on('click','.btn_excluir',function(){
            
            var edit = $(this).parents('.edit:first');
            $(this).parent().remove();
            
            var save = new saveMe(edit);
            save.verify(edit.attr('tipo'));
            delete save;
            
        });
    });
    
    /**********************************************************************************************
    * Cria layer transparente para HTML no topo do site
    **********************************************************************************************/
    $(document).off('mouseenter,mouseleave','.edit.galeria .content .cx_imagem_galeria').on('mouseenter','.edit.galeria .content .cx_imagem_galeria',function(){
        $('.edit.galeria .content .cx_imagem_galeria .layer_transparent').remove();
        if($(this).find('.layer_transparent').length == 0){
            $(this).prepend('<div class="layer_transparent" style="z-index:1;cursor:pointer" ></div>');
        }
    }).mouseleave(function(){
        $('.edit.galeria .content .cx_imagem_galeria .layer_transparent').remove();
    });
    
    $(document).off('click','.edit.galeria .content .cx_imagem_galeria .layer_transparent').on('click','.edit.galeria .content .cx_imagem_galeria .layer_transparent',function(){
        if( $(this).parent().find('.cx_text').length > 0 ){
            $('.edit.galeria .content .cx_imagem_galeria .cx_text').remove();
        }else{
            $('.edit.galeria .content .cx_imagem_galeria .cx_text').remove();
            var cx_imagem_galeria = $(this).parent();
            var cx_text  = '<div class="cx_text" contenteditable="false">';
                cx_text += '    <span class="seta_cima_cinza"></span>';
                cx_text += '    <span class="label" >Rótulo da imagem</span>';
                cx_text += '    <input type="text" value="" class="cx_text_title" placeholder="Título" />';
                cx_text += '    <textarea placeholder="Descrição" ></textarea>';
                cx_text += '    <button class="cx_text_save">Salvar</button>';
                cx_text += '    <button class="cx_text_open_image">Ver</button>';
                cx_text += '    <button class="cx_text_cancel">Cancelar</button>';
                cx_text += '    <span class="loading" style="display:none">'+ img_loading +' Salvando...</span>';
                cx_text += '</div>';

            cx_imagem_galeria.append(cx_text);
            cx_imagem_galeria.find('.cx_text .cx_text_title').val(cx_imagem_galeria.find('a[rel^=prettyPhoto] img').attr('alt'));
            cx_imagem_galeria.find('.cx_text textarea').val(cx_imagem_galeria.find('a[rel^=prettyPhoto] img').attr('title'));
        }
        
    });
    
    $(document).off('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_save').on('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_save',function(){
        var cx_galeria = $(this).parents('.cx_imagem_galeria');
        var title                   = cx_galeria.find('.cx_text_title').val();
        var text                   = cx_galeria.find('textarea').val();
        var intervalo_save_gallery = null;
             
        cx_galeria.find('a[rel^=prettyPhoto] img').attr('alt',title).attr('title',text);
        var save = new saveMe($('.edit.galeria.selected'));
        save.verify($('.edit.galeria.selected').attr('tipo'));
        delete save;
        
        cx_galeria.find('.cx_text button').hide('fast');
        cx_galeria.find('.cx_text .loading').show('fast');
        
        intervalo_save_gallery = setInterval(function(){
            cx_galeria.find('.cx_text').hide('fast',function(){$(this).remove()});
            clearInterval(intervalo_save_gallery);
            intervalo_save_gallery = null;
        },1000);
    });
    
    $(document).off('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_cancel').on('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_cancel',function(){
        $(this).parents('.cx_imagem_galeria').find('.cx_text').remove();
    });
    
    $(document).off('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_open_image').on('click','.edit.galeria .content .cx_imagem_galeria .cx_text .cx_text_open_image',function(){
        var cx_galeria = $(this).parents('.cx_imagem_galeria');
        var title      = cx_galeria.find('.cx_text_title').val();
        var text       = cx_galeria.find('textarea').val();
        var image      = cx_galeria.find('a[rel^=prettyPhoto] img').attr('src');
        var tipo       = cx_galeria.find('a[rel^=prettyPhoto] img').attr('tipo');
        
        cx_galeria.find('a[rel^=prettyPhoto] img').attr('alt',title).attr('title',text);
        
        if(tipo === 'img'){
            image = image.replace('/thumb','');
        }else if(tipo === 'video'){
            image = cx_galeria.find('a[rel^=prettyPhoto]').attr('href');
        }
        
        $.prettyPhoto.open(image,title,text);
    });
    
    
}
