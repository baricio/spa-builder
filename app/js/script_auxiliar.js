var mobileDetect = new MobileDetect(window.navigator.userAgent);
$('document').ready(function(){

        if(mobileDetect.mobile() !== null && isResponsivo()) {
            menuMobile.init();
            imagemMobile.init();
        }
    
        //Remove contenteditable das divs
        $('[contenteditable]').each(function(){
            $(this).attr('contenteditable',false);
        });
        
         //Remove layer_transparent de widgets
        $('.edit .content .layer_transparent').remove();
    
        //adiciona class ativo no menu referente a pagina de acesso atual
        var url = document.URL;  
        var partes = url.split('/');  
        var pagina = partes[partes.length - 1].replace('.php','');
        
            //remove query string params do link
            if(pagina.indexOf('?') != -1){
                pagina = pagina.substring(0, pagina.indexOf('?'));//remove query string params do link
            }
            
        if(pagina == ''){// se não encontrar a pagina vai para a pagina index
            $('li[paginaid=index]').find('a').addClass('ativo');
        }else{
            $('li[paginaid='+ pagina +']').find('a').addClass('ativo');
        }
     
        //remove scritps das imagens do site 
        ajusta_imagem();
     
        //Botao para gerar captcha do formulário
        $('form.cxformulario').each(function(){
            var form  = $(this);
            $('.captcha').on('click','img',function(){
                gera_captcha(form);
            });
        });
        
        //envio do formulário
        $('.btn_enviar').click(function(e){

                e.preventDefault();
                
                var form       = $(this).closest('form');
                var form_valid = true;
                
                form.find('label').each(function(){				
                        if(!valida_campo( $(this) )){
                            form_valid = false;
                        }
                });
                
                form.find('fieldset[field_type]').each(function(){				
                        if(!valida_campo( $(this) )){
                            form_valid = false;
                        }
                });
                
                if(typeof resizeIframe == 'function')
                {
                    resizeIframe();
                }
                
                if(form_valid === true){
                        form.attr('action','envia_form.php');
                        form.attr('method','POST');
                        //form.submit();
                        $.ajax({
                           url: 'envia_form.php',
                           dataType: 'json',
                           type: 'POST',
                           data: form.serialize(),
                           beforeSend: function(){
                               form.append('<div class="enviando" style="float: right;padding: 17px 0 0 0;"><img src="images/circle-loader-mini.gif" /></div>');
                               form.find('button[type=submit]').attr('disabled','true').css({'opacity':'0.5','filter':'alpha(opacity=50)'});
                           },
                           success:function(data){
                                if(data.captcha == 0){
                                    alert('erro captcha');
                                }else if(data.error == 1){
                                    alert('Email, não pode ser enviado, erro: ' + data.msg);
                                }else{
                                    alert(data.msg);
                                }
                                gera_captcha(form);
                                form.find('button[type=submit]').removeAttr('disabled').removeAttr('style');
                                form.find('.enviando').remove();
                           },
                           error:function(){
                               alert('Não foi possível enviar, favor tentar novamente.');
                               gera_captcha(form);
                               form.find('button[type=submit]').removeAttr('disabled').removeAttr('style');
                               form.find('.enviando').remove();
                           }
                        });
                }else{
                    gera_captcha(form);
                }

        });

});


function ajusta_imagem(){
    $('#meio').find('img').each(function(){
        
        if($(this).closest('.resize').data('ui-resizable')){
            $(this).closest('.resize').resizable('destroy');
        }

        if($(this).parent().data('ui-draggable')){
            $(this).parent().draggable('destroy');
        }

        $(this).parent().parent().parent().find('.menu_zoom').hide();
        $(this).parent().css({'cursor': ''});
        $(this).parent().parent().css({'border-right':''});
        $(this).parent().parent().css({'border-bottom':''});
        
    });
    
}

function gera_captcha(form){
    
    var image = document.createElement('img'); // new Image(1, 1); 
    image.src = 'captcha.php?' + Math.floor((Math.random()*999999999)+1);
    form.find('.captcha').find('img').replaceWith(image);
    console.log('entrou em form captcha');
    
}


function valida_campo(element_label){

        var REG_EMPTY = new RegExp('([^\s])');
        var REG_EMAIL = new RegExp('^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$');

        var field_type     = element_label.attr('field_type');
        var field_required = element_label.attr('required');
        var value 	   = ''
        var validate       = true;
        var msg            = [];
        
        if(field_type === 'text' && field_required == 'required'){

                value = element_label.find('input').val();

                if(!REG_EMPTY.test(value)){
                        validate = false;
                        msg.push({
                                id: element_label.find('input').attr('name'),
                                nome: element_label.text,
                                msg: 'Campo precisa ser preenchido'
                        });
                        element_label.find('input').css('border','1px solid red');

                        if(element_label.find('span').length == 0){
                                element_label.append('<span class="erro_msg">*Campo obrigatório</span>');
                        }
                }else{
                        element_label.find('input').attr('style','');
                        element_label.find('span').remove();
                }

        }else if(field_type === 'email'){

                value = element_label.find('input').val();

                if(!REG_EMAIL.test(value) && field_required == 'required'){
                        validate = false;
                        msg.push({
                                id: element_label.find('input').attr('id'),
                                nome: element_label.text,
                                msg: 'Email estão fora do padrão'
                        });
                        element_label.find('input').css('border','1px solid red');

                        if(element_label.find('span').length == 0){
                                element_label.append('<span class="erro_msg">*Email está fora do padrão</span>');
                        }
                }else{
                        element_label.find('input').attr('style','');
                        element_label.find('span').remove();
                }

        }else if(field_type === 'textarea' && field_required == 'required'){

                value = element_label.find('textarea').val();
                if(!REG_EMPTY.test(value)){
                        validate = false;
                        msg.push({
                                id: element_label.find('textarea').attr('id'),
                                nome: element_label.text,
                                msg: 'Campo precisa ser preenchido'
                        });
                        element_label.find('textarea').css('border','1px solid red');

                        if(element_label.find('span').length == 0){
                                element_label.append('<span class="erro_msg">*Campo obrigatório</span>');
                        }
                }else{
                        element_label.find('textarea').attr('style','');
                        element_label.find('span').remove();
                }

        }else if( (field_type === 'check' || field_type === 'radio' ) && field_required == 'required'){
                if(element_label.find('input:checked').length == 0){
                        validate = false;
                        msg.push({
                                id: element_label.find('input:first').attr('name'),
                                nome: element_label.text,
                                msg: '*Marque ao menos uma opção'
                        });
                        element_label.css('border','1px solid red');

                        if(element_label.find('span').length == 0){
                                element_label.append('<span class="erro_msg">*Marque ao menos uma opção</span>');
                        }
                }else{
                        element_label.attr('style','');
                        element_label.find('span').remove();
                }

        }

        return validate;
}

function getBlocosBlogCategoria(value,acao,widget_id){
    $('document').ready(function(){
        $.ajax({
            url: 'blog/blog_return.php',
            dataType: 'html',
            type: 'POST',
            data: {acao:acao,value:value},
            success:function(data){
                $('div[widget='+widget_id+']').parent('.content').html(data);
            },
            error:function(){
                //alert('Falha ao buscar dados do blog')
                console.log('Falha ao buscar dados do blog')
            }
        });
    });
    
}

function getBlocosBlogPost(dadosPost,dadosPaginacao,total,acao,widget_id){
    $('document').ready(function(){
        $.ajax({
            url: 'blog/blog_return.php',
            dataType: 'html',
            type: 'POST',
            data: {acao:acao,dadosPost:dadosPost,dadosPaginacao:dadosPaginacao,total:total},
            success:function(data){
                $('div[widget='+widget_id+']').parent('.content').html(data);
            },
            error:function(){
                //alert('Falha ao buscar dados do blog')
                console.log('Falha ao buscar dados do blog')
            }
        });
    });
    
}

// Verifica se a variavel está vazia inpendente do tipo, por exmplo
// undefined, null, false, number 0, empty string,
// string "0", objects sem propriedade e empty arrays
// são considerados vazios
function empty (mixed_var) {
  // *     example 1: empty(null);
  // *     returns 1: true
  // *     example 2: empty(undefined);
  // *     returns 2: true
  // *     example 3: empty([]);
  // *     returns 3: true
  // *     example 4: empty({});
  // *     returns 4: true
  // *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
  // *     returns 5: false
  var undef, key, i, len;
  var emptyValues = [undef, null, false, 0, "", "0"];

  for (i = 0, len = emptyValues.length; i < len; i++) {
    if (mixed_var === emptyValues[i]) {
      return true;
    }
  }

  if (typeof mixed_var === "object") {
    for (key in mixed_var) {
      // TODO: should we check for own properties only?
      //if (mixed_var.hasOwnProperty(key)) {
      return false;
      //}
    }
    return true;
  }

  return false;
}

//verifica se as imagens estão carregadas 
//modo de uso
//preloadimages(['1.gif', '2.gif', '3.gif']).done(function(images){
    //call back codes
//})
function preloadimages(arr,element){
    
    var newimages=[], loadedimages=0
    var postaction=function(){}
    var arr=(typeof arr!="object")? [arr] : arr    
    function imageloadpost(){
        loadedimages++
        if (loadedimages==arr.length){
            postaction(newimages,element) //call postaction and pass in newimages array as parameter
        }
    }
    
    for (var i=0; i<arr.length; i++){
        newimages[i]=new Image()
        newimages[i].src=arr[i]
        newimages[i].onload=function(){
            imageloadpost()
        }
        newimages[i].onerror=function(){
            imageloadpost();
        }
    }
    
    return { //return blank object with done() method
        done:function(f){
            postaction=f || postaction //remember user defined callback functions to be called when images load
        }
    }
}

//ajusta para que todas as colunas tenham a mesma altura
function ajutesAlturaColunas(){            
    //$('.coluna_curta,.coluna_mini,.coluna_pequena,.coluna_metade,.coluna_media,.coluna_grande').css('min-height','');
    //$('.coluna_curta,.coluna_mini,.coluna_pequena,.coluna_metade,.coluna_media,.coluna_grande').find('.columns').css('min-height','');

    var clMaxH = 0;
    var altura = new Array();
    var ordem;
    $('.coluna_curta,.coluna_mini,.coluna_pequena,.coluna_metade,.coluna_media,.coluna_grande').each(function(){
        if(ordem !== $(this).attr('ordem')){
            clMaxH = 0;
            ordem = $(this).attr('ordem');
            altura[ordem] = $(this).height();
        }

        clMaxH = $(this).height();;

        if(clMaxH > altura[ordem]){
            altura[ordem] = clMaxH;
        }

    });

    var i = 0;
    for(i = 0;i<altura.length;i++){
        $('.columns[ordem='+ i +']').css('min-height',altura[i]);
        $('.columns[ordem='+ i +']').parent().css('min-height',altura[i]);
    }
}

var facebookItensRender = 0;
function notificaFacebookRender(param){
    facebookItensRender++;
    

    
    if($('.edit.facebook').length == facebookItensRender && (mobileDetect.mobile() === null)){
        window.setTimeout(ajutesAlturaColunas(),3000);
    }
}

function isResponsivo(){

    return ($('.container-fluid').length);
}