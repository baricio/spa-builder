/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function newSlides(element,dados){
    
    var element_edit = $(element).parents('.edit.carrossel');
    
    var autoplay   = element_edit.find('input[name=autoplay]').val();
    var velocidade = (element_edit.find('input[name=velocidade]').val() * 1000);
    var controler  = element_edit.find('input[name=controler]').val();
    var random     = element_edit.find('input[name=random]').val();
    
    if(empty(dados)){
        dados = {
            auto: (autoplay == 1)? true : false,
            pager: false,
            nav:  (controler == 1)? true : false,
            random:(random == 1)? true : false, 
            pause: true,
            pauseControls: true,
            speed: 500, //velocidade do efeito de transi��o do slide
            timeout: velocidade, //velocidade de um banner para o outro
            namespace: "sliderIntro",
            before: function(data){
               
            },
            after: function(data){
               
            } // depois do efeito
        };
    }    
    
    $(element).responsiveSlides(dados);
    
    var images = "";
    $(element).find('li img').each(function(i,val){
        images += $(val).attr('src') +','; 
    });
    images = images.replace(/,$/, "");
    images = images.split(',');
    
    preloadimages(images,element).done(function(val, element){
        //pega a maior altura
        var altura_div  = $(element).parents('.content').height();
        var largura_div = $(element).parents('.content').width();
        var background  = $(element).parents('.edit.carrossel').find('input[name=background]').val();
        var i,altura    = 0;
        
        if(empty($(element).parents('.content').attr('sizeset'))){
            if($(element).parents('.tag').length > 0){
                $(element).parents('.content').find('li img').hide();
                largura_div = $(element).parents('.tag').width();
                $(element).parents('.content').css('height',altura_div);
                $(element).parents('.content').css('width',largura_div);
                $(element).parents('.content').find('li img').show();
            }else if($(element).parents('.columns').length > 0){
                $(element).parents('.content').find('li img').hide();
                largura_div = $(element).parents('.columns').width();
                $(element).parents('.content').css('height',altura_div);
                $(element).parents('.content').css('width',largura_div);
                $(element).parents('.content').find('li img').show();
            }else if($(element).parents('.topdown').length > 0){
                $(element).parents('.content').find('li img').hide();
                altura_div  = $(element).parents('.topdown').height() / 2;
                largura_div = $(element).parents('.topdown').width() / 2;
                $(element).parents('.content').css('height',altura_div);
                $(element).parents('.content').css('width',largura_div);
                $(element).parents('.content').find('li img').show();
            }
            $(element).parents('.content').css('background',background);
            $(element).parents('.content').attr('sizeset',1);
        }
        
        calcSlideSpace($(element).parents('.edit.carrossel:first'));
        
        if(!empty($(element).parents('.content').attr('save')) || !empty($(element).parents('.edit.carrossel:first').attr('new'))){
            var save = new saveMe($(element).parents('.edit.carrossel:first'));
            save.verify($(element).parents('.edit.carrossel:first').attr('tipo'));
            delete save;
            $(element).parents('.content').removeAttr('save');
        }
        
    });
    
    removeResizableSlider();
}

function newSlidesExport(element,dados){
    
    var element_edit = $(element).parents('.edit.carrossel');
    
    var autoplay   = element_edit.find('input[name=autoplay]').val();
    var velocidade = (element_edit.find('input[name=velocidade]').val() * 1000);
    var controler  = element_edit.find('input[name=controler]').val();
    var random     = element_edit.find('input[name=random]').val();
    
    if(empty(dados)){
        dados = {
            auto: (autoplay == 1)? true : false,
            pager: false,
            nav:  (controler == 1)? true : false,
            random:(random == 1)? true : false, 
            pause: true,
            pauseControls: true,
            speed: 500, //velocidade do efeito de transi��o do slide
            timeout: velocidade, //velocidade de um banner para o outro
            namespace: "sliderIntro"
        };
    }    
    
    $(element).responsiveSlides(dados);
    
    var images = "";
    $(element).find('li img').each(function(i,val){
        images += $(val).attr('src') +','; 
    });
    images = images.replace(/,$/, "");
    images = images.split(',');
    
    preloadimages(images,element).done(function(val, element){
        //pega a maior altura
        var altura_div  = $(element).parents('.content').height();
        var largura_div = $(element).parents('.content').width();
        var i,altura    = 0;
        
        calcSlideSpace($(element).parents('.edit.carrossel:first'));
        
    });
    
}

function calcSlideSpace(element_carrossel){
    
    var altura_div  = $(element_carrossel).find('.content').height();
    var altura=0;

    $(element_carrossel).find('.content').find('li img').each(function(i,val){
        altura = $(this).height();
        if(altura < altura_div){
            $(this).css('top',altura_div/2);
            $(this).css('margin-top', -1 * (altura/2));
        }else{
            $(this).removeAttr('style');
        }
    });
    
}

function resizableSlider(element){
    
    var intervalo_save_background;
    var max_width; 
    var max_height; 
    var define_para;
    
    if($(element).parents('.columns:first').length > 0){//corpo do site
        max_width   = $(element).width();
        max_height  = null;
        define_para = 'intro';
    }else if($(element).parents('.topdown').length > 0){//cabecalho ou rodape
        max_width   = $(element).parents('.topdown').width();
        max_height  = $(element).parents('.topdown').height();
        define_para = 'cabecalho';
    }
    
    $(element).find('.content').resizable({
         handles: "e,s,se",
         maxWidth: max_width,
         maxHeight: max_height,
         minHeight:'61',
         resize:function(){
            calcSlideSpace($(this).parents('.edit.carrossel:first'));
            
            if(define_para === 'cabecalho'){
                ajustePositionTopDown($(this).parents('.edit.carrossel:first'));
            }
            
            var element_content = $(this);
            
            clearInterval(intervalo_save_background);
            intervalo_save_background = setInterval(function(){
                if(define_para === 'cabecalho'){
                    ajustePositionTopDown(element_content.parents('.edit.carrossel:first'));

                    var save = new saveMe($('#topo'));
                    save.verify($('#topo').attr('id'));

                    save = new saveMe($('#rodape'));
                    save.verify($('#rodape').attr('id'));
                    delete save;
                }else{
                    var save = new saveMe(element_content.parents('.edit.carrossel:first'));
                    save.verify(element_content.parents('.edit.carrossel:first').attr('tipo'));
                    delete save;
                }
                
                clearInterval(intervalo_save_background);
                intervalo_save_background = null;
            },500);
             
         },
         stop: function(){
     
            if(define_para === 'cabecalho'){
                ajustePositionTopDown($(this).parents('.edit.carrossel:first'));
                
                var save = new saveMe($('#topo'));
                save.verify($('#topo').attr('id'));

                save = new saveMe($('#rodape'));
                save.verify($('#rodape').attr('id'));
                delete save;
            }else{
                var save = new saveMe($(this).parents('.edit.carrossel:first'));
                save.verify($(this).parents('.edit.carrossel:first'));
                delete save;
            }
            
         }
    });
}

function removeResizableSlider(){
    $('.edit.carrossel .content').each(function(){
        if($(this).data('ui-resizable')){
            $(this).resizable('destroy');
        }
    });
}