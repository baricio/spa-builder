/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function relative_time(time_value){
        var values = time_value.split(" ");
        time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
        var parsed_date = Date.parse(time_value);
        var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
        var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
        delta = delta + (relative_to.getTimezoneOffset() * 60);

        if (delta < 60){
                return 'menos de um minuto atrás';
        } else if(delta < 120) {
                return 'cerca de um minuto atrás';
        } else if(delta < (60*60)) {
                return (parseInt(delta / 60)).toString() + ' minutos atrás';
        } else if(delta < (120*60)) {
                return 'cerca de uma hora atrás';
        } else if(delta < (24*60*60)) {
                return 'about ' + (parseInt(delta / 3600)).toString() + ' horas atrás';
        } else if(delta < (48*60*60)) {
                return '1 dia atrás';
        } else {
                return (parseInt(delta / 86400)).toString() + ' dias atrás';
        }
}

function retornaTwitter(element, user){
        $.getJSON("http://cms.fcsoftware.com.br/twitter/tweets/get/"+user,function (dados) {

                var statusHTML = [];
                var fallow = '';

                $.each(dados,function(key,val){
                        var username = val.user.screen_name;
                        var imgUserUrl = val.user.profile_image_url;
                        var status = val.text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url){
                                return '<a href="'+url+'" target="_blank">'+url+'</a>';
                        }).replace(/\B@([_a-z0-9]+)/ig, function(reply){
                                return reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'" target="_blank">'+reply.substring(1)+'</a>';
                        });
                        var time_pass = relative_time(val.created_at)

                        statusHTML.push(
                                '<li>' +
                                '<img src="'+imgUserUrl+'" /><span>'+status+'</span>' + 
                                '<br /><a target="_blank" href="http://twitter.com/'+username+'/status/'+val.id_str+'" class="published">'+time_pass+'</a>'+
                                '</li>'
                        );

                });
                fallow = '<h2>Tweets</h2><a href="https://twitter.com/'+user+'" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" data-lang="pt">Seguir @'+user+'</a>';
                element.html(fallow + '<ul>' + statusHTML.join('') + '</ul>');
                element.parent().find('input[name=user]').val(user);
                
                twttr.widgets.load();
        });
}
