'use strict';

angular.module('myApp.route', ['ui.router'])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home',{url: '/home', templateUrl: 'views/site/home.html', controller: 'homeCtrl'})
    .state('aong',{url: '/a-ong', templateUrl: 'views/site/a-ong.html', controller: 'homeCtrl'})
    .state('midia',{url: '/midia', templateUrl: 'views/site/midia.html', controller: 'homeCtrl'})
    .state('news',{cache: false, url: '/news/:siteId/:pageId?categoriaId&postId&paginate', templateUrl: 'views/site/news.html', controller: 'blogCtrl'})    
    .state('contato',{url: '/contato', templateUrl: 'views/site/contato.html', controller: 'homeCtrl'})
    
    $urlRouterProvider.otherwise('/home');
}).run( function($rootScope, $location, chat) {

  $rootScope.$on('$viewContentLoaded', function () {
    $("a[rel^='prettyPhoto']").prettyPhoto({deeplinking:false});
    activeClickButtonCaptcha();
    activeSendEmailButton();
    ajutesAlturaColunas();

    chat.getLive().success(function(data){
      if(data.success){
        widgetChat
        .init({ url: data.url, token: data.token})
        .createLive();
      }
    })

    chat.getSite().success(function(data){
      if(data.success){
        widgetChat
        .init({ url: data.url, token: data.token})
        .createChat();
      }
    })

  })

});

function activeClickButtonCaptcha() {
    //Botao para gerar captcha do formulário
    $('form.cxformulario').each(function(){
        var form  = $(this);
        $('.captcha').on('click','img',function(){
            gera_captcha(form);
        });
    });
}

function activeSendEmailButton() {
    $('.btn_enviar').click(function(e){

      e.preventDefault();
      
      var form       = $(this).closest('form');
      var form_valid = true;
      
      form.find('label').each(function(){       
              if(!valida_campo( $(this) )){
                  form_valid = false;
              }
      });
      
      form.find('fieldset[field_type]').each(function(){        
              if(!valida_campo( $(this) )){
                  form_valid = false;
              }
      });
      
      if(typeof resizeIframe == 'function')
      {
          resizeIframe();
      }
      
      if(form_valid === true){
              form.attr('action','envia_form.php');
              form.attr('method','POST');
              //form.submit();
              $.ajax({
                 url: 'envia_form.php',
                 dataType: 'json',
                 type: 'POST',
                 data: form.serialize(),
                 beforeSend: function(){
                     form.append('<div class="enviando" style="float: right;padding: 17px 0 0 0;"><img src="images/circle-loader-mini.gif" /></div>');
                     form.find('button[type=submit]').attr('disabled','true').css({'opacity':'0.5','filter':'alpha(opacity=50)'});
                 },
                 success:function(data){
                      if(data.captcha == 0){
                          alert('erro captcha');
                      }else if(data.error == 1){
                          alert('Email, não pode ser enviado, erro: ' + data.msg);
                      }else{
                          alert(data.msg);
                      }
                      gera_captcha(form);
                      form.find('button[type=submit]').removeAttr('disabled').removeAttr('style');
                      form.find('.enviando').remove();
                 },
                 error:function(){
                     alert('Não foi possível enviar, favor tentar novamente.');
                     gera_captcha(form);
                     form.find('button[type=submit]').removeAttr('disabled').removeAttr('style');
                     form.find('.enviando').remove();
                 }
              });
      }else{
          gera_captcha(form);
      }

    });
}

function ajutesAlturaColunas() {

    var clMaxH = 0;
    var altura = new Array();
    var ordem;
    $('.coluna_curta,.coluna_mini,.coluna_pequena,.coluna_metade,.coluna_media,.coluna_grande').each(function(){
        if(ordem !== $(this).attr('ordem')){
            clMaxH = 0;
            ordem = $(this).attr('ordem');
            altura[ordem] = $(this).height();
        }

        clMaxH = $(this).height();;

        if(clMaxH > altura[ordem]){
            altura[ordem] = clMaxH;
        }

    });

    var i = 0;
    for(i = 0;i<altura.length;i++){
        $('.columns[ordem='+ i +']').css('min-height',altura[i]);
        $('.columns[ordem='+ i +']').parent().css('min-height',altura[i]);
    }
}